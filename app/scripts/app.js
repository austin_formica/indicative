'use strict';

/**
 * @ngdoc overview
 * @name indicativeApp
 * @description
 * # indicativeApp
 *
 * Main module of the application.
 */
angular
  .module('indicativeApp', [
    'ngRoute',
    'highcharts-ng'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

angular.module('indicativeApp').filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
      return $filter('number')(input * 100, decimals) + '%';
    };
  }]);
