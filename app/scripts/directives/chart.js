'use strict';

/**
 * @ngdoc directive
 * @name indicativeApp.directive:chart
 * @description
 * # chart
 */

angular.module('indicativeApp')
  .directive('chart', function () {
  return {
    restrict: 'E',
    scope: {
      series: '=info',
      categories: '=categories'
    },
    bindToController: 'MainCtrl',
    template: '<div id="container"  style="max-width: 70%;float:left;"></div>',
    link: function (scope) {
      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'container',
        },
        title: {
          text: "Purchases by Date"
        },
        xAxis: {
          categories: scope.categories
        },
        yAxis: {
          title: {
            text: 'User Count'
          },
          plotLines: [{
            value: 0
          }]
        },
        series: scope.series,
      });
    }
  };//return
});
