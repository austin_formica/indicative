'use strict';

/**
 * @ngdoc service
 * @name indicativeApp.ChartService
 * @description
 * # ChartService
 * Service in the indicativeApp.
 */
angular.module('indicativeApp')
  .service('ChartService', function () {

    function chartConfig(cats) {
      return {
        options: {
            chart: {
                type: 'line'
            }
        },
        series: [],
        title: {
            text: 'Purchases'
        },
        xAxis: {
          categories: cats
        },
        yAxis: {
          title: {text: 'Purchases'}
        },
        loading: false
      };
    }

    function plotColorsChart(data, dates, colors) {
      var chart = matchColor(data, dates, colors);
      console.log(chart);
      return chart;
    }

    function matchColor(data, dates, colors) {
      var output = [];
      colors.forEach(function(color) {
        var match = matchColorPurchases(data, dates, color);
        output.push(match);
      });
      return output;
    }

    function matchColorPurchases(data, dates, color) {
      var array = [];
      dates.forEach(function(date) {
        var counter = 0;
        for (var i = 0; i < data.length; i++) {
          if(data[i].purchased_item && data[i].purchased_item.color) {
            if (data[i].purchased_item.date === date && data[i].purchased_item.color === color) {
              counter++;
            }
          }
        }
        array.push(counter);
      });
      var obj = {
        name: color,
        data: array
      };
      return obj;
    }


    function plotPurchasesChart(data, arr) {
      var dataSet = [];
      arr.forEach(function(date) {
        var dayCount = matchDate(data, date);
        dataSet.push(dayCount);
      });
      return dataSet;
    }

    function matchDate(data, date) {
      var count = 0;
      data.forEach(function(obj) {
        if (obj.purchased_item && obj.purchased_item.date) {
          if (obj.purchased_item.date === date) {
            count++;
          }
        }
      });
      return count;
    }

    return {
      chartConfig: chartConfig,
      plotColorsChart: plotColorsChart,
      plotPurchasesChart: plotPurchasesChart
    };

    // AngularJS will instantiate a singleton by calling "new" on this function
  });
