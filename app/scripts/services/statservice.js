'use strict';

/**
 * @ngdoc service
 * @name indicativeApp.StatService
 * @description
 * # StatService
 * Service in the indicativeApp.
 */
angular.module('indicativeApp')
  .service('StatService', function () {

    function colorsCalc(data) {
      var array = [];
      data.forEach(function(obj) {
        if (obj.purchased_item && obj.purchased_item.color) {
          var color = obj.purchased_item.color;
          if (array.indexOf(color) >= 0) {
            return;
          } else {
            array.push(color);
          }
        }
      });
      return array;
    }

    function genderCalc(data, gender) {
      var count = 0;
      data.forEach(function(obj) {
        if(obj.gender === gender) {
          count++;
        }
      });
      var percent = (count / data.length);
      return percent;
    }

    function actionCalc(data, act) {
      var count = 0;
      data.forEach(function(obj) {
        if(obj[act]) {
          count++;
        }
      });
      return count;
    }

    function defineConversion(data, arr) {
      var conversionCount = 0;
      for (var i = 0; i < data.length; i++) {
        var bool = conversionCalc(data[i], arr);
        if (bool) {
          conversionCount++;
        }
      }
      var percent = (conversionCount / data.length);
      return percent;
    }


    //private function
    function conversionCalc(data, arr) {
      var conversionDates = [];
      for (var i = 0; i < arr.length; i++) {
        var key = arr[i];
        if (data[key] && data[key].date) {
          conversionDates.push(data[key].date);
        }
      }
      if (conversionDates.length === arr.length) {
        if (conversionDates.length === 2) {
          var date1 = processDate(conversionDates[0]);
          var date2 = processDate(conversionDates[1]);
          if (date1 < date2) {
            return true;
          }
        } else if (conversionDates.length === 3) {
          var date1 = processDate(conversionDates[0]);
          var date2 = processDate(conversionDates[1]);
          var date3 = processDate(conversionDates[2]);
          if (date1 < date2 && date2 < date3) {
            return true;
          }
        } else {
          return false;
        }
      }
    }

    function processDate(date){
      var parts = date.split("/");
      return new Date(parts[2], parts[0] - 1, parts[1]);
    }

    //public api
    return {
      colorsCalc: colorsCalc,
      genderCalc: genderCalc,
      actionCalc: actionCalc,
      defineConversion: defineConversion
    };


  });
