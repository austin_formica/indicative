'use strict';

/**
 * @ngdoc service
 * @name indicativeApp.TableService
 * @description
 * # TableService
 * Service in the indicativeApp.
 */
angular.module('indicativeApp')
  .service('TableService', function () {
    function createTable(data, desiredKeys) {
      var myTable = {};
      //table titles
      myTable.titles = new Array(7);
      //table Obj
      myTable.objArr = [];
      for (var key in data) {
        var myObj = {};
        for (var field in data[key]) {
          var index = desiredKeys.indexOf(field);
          if (index >= 0) {
            //capitalize for table titles
            var title = field.replace('_', ' ');
            title = title.capitalize();
            myTable.titles[index] = title;
            //map new object with the desiredKeys
            myObj[field] = data[key][field];
          }
        }
        myTable.objArr.push(myObj);
      }
      return myTable;
    }

    String.prototype.capitalize = function() {
      return this.replace(/(?:^|\s)\S/g, function(a) {
        return a.toUpperCase();
      });
    };

    return {
      createTable: createTable
    };

  });
