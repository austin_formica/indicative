'use strict';

/**
 * @ngdoc service
 * @name indicativeApp.InfoService
 * @description
 * # InfoService
 * Service in the indicativeApp.
 */
angular.module('indicativeApp')
  .service('InfoService', function ($http) {

    function chartInfo() {
      return $http.get('/seed.json').then(function(res) {
        return res.data;
      });
    }

    function getRecord() {
      return $http.get('https://mockaroo.com/e7995d70/download?count=1&key=015777f0').then(function(res) {
        return res.data;
      });
    }

    return {
      chartInfo: chartInfo,
      getRecord: getRecord
    };

  });
