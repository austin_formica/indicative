'use strict';

/**
 * @ngdoc function
 * @name indicativeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the indicativeApp
 */
angular.module('indicativeApp')
  .controller('MainCtrl', function (InfoService, ChartService, StatService, TableService) {

    var vm = this;
    vm.table = {};
    vm.myTable = {};
    vm.stats = {};
    vm.displayChart = false;
    vm.showStats = true;
    var desiredKeys = ['first_name', 'last_name', 'gender', 'company_name', 'job_title', 'email', 'id'];
    vm.categories = ['12/22/2015', '12/23/2015', '12/24/2015', '12/25/2015', '12/26/2015', '12/27/2015', '12/28/2015'];
    vm.chartConfig = ChartService.chartConfig(vm.categories);

    vm.showChart = function() {
      vm.displayChart = true;
    };

    vm.hideChart = function() {
      vm.displayChart = false;
    };

    vm.colorChart = function() {
      getColorChartData(vm.fullData);
    };

    vm.userChart = function() {
      vm.displayColorChart = false;
      setUserChart(vm.fullData);
    };

    vm.addRow = function() {
      console.log('add row');
      getRecord();
    };

    function init() {
      vm.loading = true;
      getChartData();
    }

    //get seed json
    function getChartData() {
      InfoService.chartInfo().then(function(data) {
        vm.loading = false;
        console.log(data);
        vm.fullData = data;
        scopeTableData(data);
        scopeStatsData(data);
        setUserChart(data);
      }, function(err) {
        console.log(err);
      });
    }

    //add new record
    function getRecord() {
      InfoService.getRecord().then(function(data) {
        console.log(data);
        vm.myTable.objArr.push(data[0]);
        vm.fullData.push(data[0]);
        scopeStatsData(vm.fullData);
        if (vm.displayColorChart) {
          getColorChartData(vm.fullData);
        } else {
          setUserChart(vm.fullData);
        }
      }, function(err) {
        console.log(err);
      });
    }

    //chart helpers
    function refreshSeries() {
      vm.chartConfig.series = [];
    }

    function addSeries(series) {
      vm.chartConfig.series.push({
        name: series.name,
        data: series.data
      });
    }

    //user chart
    function setUserChart(data) {
      var userChart = ChartService.plotPurchasesChart(data, vm.categories);
      refreshSeries();
      var series = {
        name: 'Purchases',
        data: userChart
      };
      addSeries(series);
    }

    //color chart
    function getColorChartData(data) {
      var colorsArr = StatService.colorsCalc(data);
      var chart = ChartService.plotColorsChart(data,vm.categories,colorsArr);
      setColorChart(chart);
    }

    function setColorChart(chart) {
      vm.displayColorChart = true;
      refreshSeries();
      for (var i = 0; i < chart.length; i++) {
        addSeries(chart[i]);
      }
    }

    //display table
    function scopeTableData(data) {
      var table = TableService.createTable(data, desiredKeys);
      vm.myTable = table;
    }

    //display stats
    function scopeStatsData(data) {
      console.log(data);

      vm.stats.totalUsers = data.length;

      var colorArr = StatService.colorsCalc(data);
      // var colorArr = colorsCalc(data);
      console.log(colorArr);
      vm.stats.totalColors = colorArr.length;

      var malePercent = StatService.genderCalc(data, 'Male');
      vm.stats.malePercent = malePercent;

      var femalePercent = StatService.genderCalc(data, 'Female');
      vm.stats.femalePercent = femalePercent;

      var signedUpCount = StatService.actionCalc(data, 'signed_up');
      vm.stats.signedUpCount = signedUpCount;

      var viewedProfileCount = StatService.actionCalc(data, 'viewed_profile');
      vm.stats.viewedProfileCount = viewedProfileCount;

      var viewedItemCount = StatService.actionCalc(data, 'viewed_item');
      vm.stats.viewedItemCount = viewedItemCount;

      var purchasedItemCount = StatService.actionCalc(data, 'purchased_item');
      vm.stats.purchasedItemCount = purchasedItemCount;

      var conversionOne = StatService.defineConversion(data, ['signed_up', 'purchased_item']);
      vm.stats.conversionOne = conversionOne;

      var conversionTwo = StatService.defineConversion(data, ['signed_up', 'viewed_profile', 'purchased_item']);
      vm.stats.conversionTwo = conversionTwo;

      var conversionThree = StatService.defineConversion(data, ['signed_up', 'viewed_item', 'purchased_item']);
      vm.stats.conversionThree = conversionThree;

      console.log(vm.stats);
    }

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    init();
  });
