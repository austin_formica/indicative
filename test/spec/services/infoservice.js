'use strict';

describe('Service: InfoService', function () {

  // load the service's module
  beforeEach(module('indicativeApp'));

  // instantiate service
  var InfoService;
  beforeEach(inject(function (_InfoService_) {
    InfoService = _InfoService_;
  }));

  it('should do something', function () {
    expect(!!InfoService).toBe(true);
  });

});
