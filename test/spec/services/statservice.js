'use strict';

describe('Service: StatService', function () {

  // load the service's module
  beforeEach(module('indicativeApp'));

  // instantiate service
  var StatService;
  beforeEach(inject(function (_StatService_) {
    StatService = _StatService_;
  }));

  it('should do something', function () {
    expect(!!StatService).toBe(true);
  });

});
